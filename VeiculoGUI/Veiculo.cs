﻿namespace VeiculoGUI
{
    using System;

    public class Veiculo
    {
        #region Atributos

        private string _matricula; // Matricula
        private readonly double _kmTotal; // Total Km percuridos (Km)
        private double _capacidade; // Capacidade do deposido (Litro)
        private readonly double _quantidade; // Quantidade de combustivel no deposito (Litro)
        private double _contador; // Contagem de quilometros (Km)
        private double _consumoMedio100k; // Média de consumo aos 100 Km
        private bool _semCombustivel; // Bool para verificar se veiculo está sem combustivel
        private bool _limiteCombustivel; // Bool para verificar se deposito está cheio
        private int _viagem; // Contar as viagens
        private readonly double _capacidadeReserva = 10.0; // Capacidade da reserva que é retirado da capacidade do deposido (Litro)
        private bool _reserva; // Bool para verifivar se entrou na reserva
        #endregion

        #region Propriedades

        public string Matricula { get; set; }
        public double KmTotal { get; set; }
        public double Capacidade { get; set; }
        public double Quantidade { get; private set; }
        public double Contador { get; set; }
        public double ConsumoMedio100Km { get; set; }
        public bool SemCombustivel { get; private set; }
        public bool LimiteCombustivel { get; private set; }
        public int Viagem { get; private set; }

        #endregion

        #region Construtores

        #endregion

        #region Metodos

        private void VerificarSeTemCombustivel()
        {
            SemCombustivel = false;

            double consumo;

            consumo = Quantidade - (Contador * ConsumoMedio100Km) / 100;

            if (consumo < 0)
            {
                SemCombustivel = true;
            }
            else
            {
                Quantidade = Math.Round(consumo, 2);
            }

        }
        public void QuilometrosSoma(double valor)
        {

            VerificarSeTemCombustivel();

            if (!SemCombustivel)
            {
                KmTotal = Math.Round(KmTotal + valor,2);

                Viagem++;
            }
        }
        public bool VerificacaoReserva()
        {
            if (Quantidade <= _capacidadeReserva)
            {
                _reserva = true;
            }
            else
            {
                _reserva = false;
            }
            return _reserva;
        }
        public double Abastecer(double valor)
        {
            double qttFinal;
            LimiteCombustivel = false;

            qttFinal = Quantidade + valor;

            if (qttFinal <= Capacidade)
            {
                Quantidade = qttFinal;
            }
            else
            {
                LimiteCombustivel = true;
            }

            return Quantidade;
        }
        public double QuantosKmPodePercurrer ()
        {
            double kmPossiveis;

            kmPossiveis = Math.Round((Quantidade * 100) / ConsumoMedio100Km,2);

            return kmPossiveis;
        }
        public double CustoViagem (double kmPercurridos, double custoLitro)
        {
            return kmPercurridos * custoLitro;
        }
        public double CustoMedioKm (double custoLitro)
        {
            return (ConsumoMedio100Km / 100) * custoLitro;
        }


        #endregion


    }
}



