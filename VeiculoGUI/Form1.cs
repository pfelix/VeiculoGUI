﻿namespace VeiculoGUI
{

    using System;
    using System.Drawing;
    using System.Text.RegularExpressions;
    using System.Windows.Forms;

    public partial class Form1 : Form
    {
        Veiculo myVeiculo;
        double _mediaCustoLitro; // Média custo por litro (€)

        public Form1()
        {
            InitializeComponent();
            myVeiculo = new Veiculo();
            LabelKmPossiveis.Text = "";
            LabelCustoViagem.Text = "";
            LabelCustoMedioKm.Text = "";
            LabelReserva.Text = "- -";
            LabelViagem.Text = "";
        }

        private void ButtonRegistarCarro_Click(object sender, EventArgs e)
        {
            Regex regexMatricula = new Regex("^[0-9]{2}-[a-zA-Z]{2}-[0-9]{2}$");
            Match matchMatricula = regexMatricula.Match(TextBoxMatricula.Text);
            double capacidade;
            double consumiMedio100Km;
            double kmInicial;

            if (!double.TryParse(TextBoxCustoMedioLitro.Text, out _mediaCustoLitro)) // Valida se Preço do litro é double
            {
                MessageBox.Show("O valor introduzido no campo Custo médio combustivel por litro está inválido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxCustoMedioLitro.Select();
            }
            else if (!matchMatricula.Success)
            {
                MessageBox.Show("O valor introduzido no campo " + LabelMatricula.Text + " está inválido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxMatricula.Select();
            }
            else if (!double.TryParse(TextBoxCapacidade.Text, out capacidade))
            {
                MessageBox.Show("O valor introduzido no campo " + LabelCapacidadeDeposito.Text + " está inválido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxCapacidade.Select();
            }
            else if (!double.TryParse(TextBoxConsumoMedio.Text, out consumiMedio100Km))
            {
                MessageBox.Show("O valor introduzido no campo " + LabelConsumoMedio100Km.Text + " está inválido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxConsumoMedio.Select();
            }
            else if (!double.TryParse(TextBoxKmInicial.Text, out kmInicial))
            {
                MessageBox.Show("O valor introduzido no campo " + LabelKmInicial.Text + " está inválido.", "Atenção", MessageBoxButtons.OK, MessageBoxIcon.Information);
                TextBoxKmInicial.Select();
            }
            else
            {
                myVeiculo.Matricula = TextBoxMatricula.Text;
                myVeiculo.Capacidade = Convert.ToDouble(TextBoxCapacidade.Text);
                myVeiculo.ConsumoMedio100Km = Convert.ToDouble(TextBoxConsumoMedio.Text);
                myVeiculo.KmTotal = Convert.ToDouble(TextBoxKmInicial.Text);
                ButtonRegistarCarro.Enabled = false;
                TextBoxKmInicial.Enabled = false;
                TextBoxCapacidade.Enabled = false;
                TextBoxMatricula.Enabled = false;
                TextBoxConsumoMedio.Enabled = false;
                TextBoxCustoMedioLitro.Enabled = false;

                TextBoxKmViagem.Enabled = true;
                ButtonRegistarKmViagem.Enabled = true;

                TextBoxKmAtual.Text = myVeiculo.KmTotal.ToString();

                LabelQttCombustivel.Text = myVeiculo.Quantidade.ToString() + "/" + myVeiculo.Capacidade.ToString();

                TextBoxQttAbastecer.Enabled = true;
                ButtonAbastecer.Enabled = true;

                LabelKmPossiveis.Text = "O veiculo pode percurer "+myVeiculo.QuantosKmPodePercurrer().ToString()+" Km com o combustivel que tem.";

            }
        }

        private void ButtonRegistarKmViagem_Click(object sender, EventArgs e)
        {
            double kmViagem;

            if (!double.TryParse(TextBoxKmViagem.Text, out kmViagem))
            {
                MessageBox.Show("O campo Km viagem tem um valor inválido.", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            else
            {
                myVeiculo.Contador = kmViagem;
                  
                myVeiculo.QuilometrosSoma(myVeiculo.Contador);

                if (!myVeiculo.SemCombustivel)
                {
                    TextBoxKmAtual.Text = myVeiculo.KmTotal.ToString();
                    LabelQttCombustivel.Text = myVeiculo.Quantidade.ToString() + "/" + myVeiculo.Capacidade.ToString();
                    LabelKmPossiveis.Text = "O veiculo pode percurer " + myVeiculo.QuantosKmPodePercurrer().ToString() + " Km com o combustivel que tem.";
                    LabelViagem.Text = "Já registou "+myVeiculo.Viagem.ToString()+" viagens";

                    if (myVeiculo.VerificacaoReserva())
                    {
                        LabelReserva.BackColor = Color.Red; 
                        LabelReserva.Text = "Na reserva";
                    }
                    else
                    {
                        LabelReserva.BackColor = SystemColors.Control;
                        LabelReserva.Text = "- -";
                    }
                }
                else
                {
                    MessageBox.Show("Sem combustivel suficiente para fazer esta viagem.\nAbasteça o veiculo.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                LabelCustoViagem.Text = "Esta viagem teve um custo de " + myVeiculo.CustoViagem(myVeiculo.Contador, _mediaCustoLitro) + " e já foi gasto " + myVeiculo.CustoViagem(myVeiculo.KmTotal, _mediaCustoLitro);
                LabelCustoMedioKm.Text = "O custo médio por quilometro é " + myVeiculo.CustoMedioKm(_mediaCustoLitro);

            }
        }
        
        private void ButtonAbastecer_Click(object sender, EventArgs e)
        {
            double qttAbastecer;

            if (double.TryParse(TextBoxQttAbastecer.Text, out qttAbastecer))
            {
                myVeiculo.Abastecer(qttAbastecer);

                if (!myVeiculo.LimiteCombustivel)
                {
                    LabelQttCombustivel.Text = myVeiculo.Quantidade.ToString() + "/" + myVeiculo.Capacidade.ToString();
                }
                else
                {
                    qttAbastecer = myVeiculo.Capacidade - myVeiculo.Quantidade;
                    MessageBox.Show("Não pode abastecer a quantidade predendida.\nSó pode abastecer "+qttAbastecer+" Litros.", "Informação", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }
            }
            if (myVeiculo.VerificacaoReserva())
            {
                LabelReserva.BackColor = Color.Red;
                LabelReserva.Text = "Na reserva";
            }
            else
            {
                LabelReserva.BackColor = SystemColors.Control;
                LabelReserva.Text = "- -";
            }
            LabelKmPossiveis.Text = "O veiculo pode percurer " + myVeiculo.QuantosKmPodePercurrer().ToString() + " Km com o combustivel que tem.";
        }
    }
}
