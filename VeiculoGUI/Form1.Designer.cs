﻿namespace VeiculoGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.TextBoxMatricula = new System.Windows.Forms.TextBox();
            this.LabelMatricula = new System.Windows.Forms.Label();
            this.LabelCapacidadeDeposito = new System.Windows.Forms.Label();
            this.TextBoxCapacidade = new System.Windows.Forms.TextBox();
            this.ButtonRegistarCarro = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.TextBoxKmInicial = new System.Windows.Forms.TextBox();
            this.LabelKmInicial = new System.Windows.Forms.Label();
            this.TextBoxCustoMedioLitro = new System.Windows.Forms.TextBox();
            this.TextBoxConsumoMedio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.LabelConsumoMedio100Km = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ButtonRegistarKmViagem = new System.Windows.Forms.Button();
            this.TextBoxKmViagem = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.TextBoxKmAtual = new System.Windows.Forms.TextBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ButtonAbastecer = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.TextBoxQttAbastecer = new System.Windows.Forms.TextBox();
            this.LabelQttCombustivel = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.LabelReserva = new System.Windows.Forms.Label();
            this.LabelKmPossiveis = new System.Windows.Forms.Label();
            this.LabelCustoViagem = new System.Windows.Forms.Label();
            this.LabelCustoMedioKm = new System.Windows.Forms.Label();
            this.LabelViagem = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // TextBoxMatricula
            // 
            this.TextBoxMatricula.Location = new System.Drawing.Point(179, 19);
            this.TextBoxMatricula.Name = "TextBoxMatricula";
            this.TextBoxMatricula.Size = new System.Drawing.Size(100, 20);
            this.TextBoxMatricula.TabIndex = 0;
            this.TextBoxMatricula.Text = "11-XX-11";
            this.TextBoxMatricula.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LabelMatricula
            // 
            this.LabelMatricula.AutoSize = true;
            this.LabelMatricula.Location = new System.Drawing.Point(7, 23);
            this.LabelMatricula.Name = "LabelMatricula";
            this.LabelMatricula.Size = new System.Drawing.Size(50, 13);
            this.LabelMatricula.TabIndex = 1;
            this.LabelMatricula.Text = "Matricula";
            // 
            // LabelCapacidadeDeposito
            // 
            this.LabelCapacidadeDeposito.AutoSize = true;
            this.LabelCapacidadeDeposito.Location = new System.Drawing.Point(7, 48);
            this.LabelCapacidadeDeposito.Name = "LabelCapacidadeDeposito";
            this.LabelCapacidadeDeposito.Size = new System.Drawing.Size(141, 13);
            this.LabelCapacidadeDeposito.TabIndex = 2;
            this.LabelCapacidadeDeposito.Text = "Capacidade deposito (Litros)";
            // 
            // TextBoxCapacidade
            // 
            this.TextBoxCapacidade.Location = new System.Drawing.Point(179, 44);
            this.TextBoxCapacidade.Name = "TextBoxCapacidade";
            this.TextBoxCapacidade.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCapacidade.TabIndex = 3;
            this.TextBoxCapacidade.Text = "55";
            this.TextBoxCapacidade.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // ButtonRegistarCarro
            // 
            this.ButtonRegistarCarro.Location = new System.Drawing.Point(179, 143);
            this.ButtonRegistarCarro.Name = "ButtonRegistarCarro";
            this.ButtonRegistarCarro.Size = new System.Drawing.Size(100, 23);
            this.ButtonRegistarCarro.TabIndex = 4;
            this.ButtonRegistarCarro.Text = "Registar";
            this.ButtonRegistarCarro.UseVisualStyleBackColor = true;
            this.ButtonRegistarCarro.Click += new System.EventHandler(this.ButtonRegistarCarro_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.TextBoxKmInicial);
            this.groupBox1.Controls.Add(this.LabelKmInicial);
            this.groupBox1.Controls.Add(this.TextBoxCustoMedioLitro);
            this.groupBox1.Controls.Add(this.TextBoxConsumoMedio);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.LabelConsumoMedio100Km);
            this.groupBox1.Controls.Add(this.ButtonRegistarCarro);
            this.groupBox1.Controls.Add(this.LabelMatricula);
            this.groupBox1.Controls.Add(this.TextBoxCapacidade);
            this.groupBox1.Controls.Add(this.LabelCapacidadeDeposito);
            this.groupBox1.Controls.Add(this.TextBoxMatricula);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(288, 173);
            this.groupBox1.TabIndex = 5;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Registar Veiculo";
            // 
            // TextBoxKmInicial
            // 
            this.TextBoxKmInicial.Location = new System.Drawing.Point(179, 119);
            this.TextBoxKmInicial.Name = "TextBoxKmInicial";
            this.TextBoxKmInicial.Size = new System.Drawing.Size(100, 20);
            this.TextBoxKmInicial.TabIndex = 10;
            this.TextBoxKmInicial.Text = "0";
            this.TextBoxKmInicial.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // LabelKmInicial
            // 
            this.LabelKmInicial.AutoSize = true;
            this.LabelKmInicial.Location = new System.Drawing.Point(10, 123);
            this.LabelKmInicial.Name = "LabelKmInicial";
            this.LabelKmInicial.Size = new System.Drawing.Size(51, 13);
            this.LabelKmInicial.TabIndex = 9;
            this.LabelKmInicial.Text = "Km inicial";
            // 
            // TextBoxCustoMedioLitro
            // 
            this.TextBoxCustoMedioLitro.Location = new System.Drawing.Point(179, 94);
            this.TextBoxCustoMedioLitro.Name = "TextBoxCustoMedioLitro";
            this.TextBoxCustoMedioLitro.Size = new System.Drawing.Size(100, 20);
            this.TextBoxCustoMedioLitro.TabIndex = 8;
            this.TextBoxCustoMedioLitro.Text = "1,3";
            this.TextBoxCustoMedioLitro.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // TextBoxConsumoMedio
            // 
            this.TextBoxConsumoMedio.Location = new System.Drawing.Point(179, 69);
            this.TextBoxConsumoMedio.Name = "TextBoxConsumoMedio";
            this.TextBoxConsumoMedio.Size = new System.Drawing.Size(100, 20);
            this.TextBoxConsumoMedio.TabIndex = 7;
            this.TextBoxConsumoMedio.Text = "7";
            this.TextBoxConsumoMedio.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(7, 98);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(161, 13);
            this.label4.TabIndex = 6;
            this.label4.Text = "Custo médio combustivel por litro";
            // 
            // LabelConsumoMedio100Km
            // 
            this.LabelConsumoMedio100Km.AutoSize = true;
            this.LabelConsumoMedio100Km.Location = new System.Drawing.Point(7, 73);
            this.LabelConsumoMedio100Km.Name = "LabelConsumoMedio100Km";
            this.LabelConsumoMedio100Km.Size = new System.Drawing.Size(152, 13);
            this.LabelConsumoMedio100Km.TabIndex = 5;
            this.LabelConsumoMedio100Km.Text = "Consumo médio 100Km (Litros)";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.ButtonRegistarKmViagem);
            this.groupBox2.Controls.Add(this.TextBoxKmViagem);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Location = new System.Drawing.Point(12, 190);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(288, 72);
            this.groupBox2.TabIndex = 6;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Registar Viagem";
            // 
            // ButtonRegistarKmViagem
            // 
            this.ButtonRegistarKmViagem.Enabled = false;
            this.ButtonRegistarKmViagem.Location = new System.Drawing.Point(175, 42);
            this.ButtonRegistarKmViagem.Name = "ButtonRegistarKmViagem";
            this.ButtonRegistarKmViagem.Size = new System.Drawing.Size(100, 23);
            this.ButtonRegistarKmViagem.TabIndex = 2;
            this.ButtonRegistarKmViagem.Text = "Registar";
            this.ButtonRegistarKmViagem.UseVisualStyleBackColor = true;
            this.ButtonRegistarKmViagem.Click += new System.EventHandler(this.ButtonRegistarKmViagem_Click);
            // 
            // TextBoxKmViagem
            // 
            this.TextBoxKmViagem.Enabled = false;
            this.TextBoxKmViagem.Location = new System.Drawing.Point(176, 16);
            this.TextBoxKmViagem.Name = "TextBoxKmViagem";
            this.TextBoxKmViagem.Size = new System.Drawing.Size(99, 20);
            this.TextBoxKmViagem.TabIndex = 1;
            this.TextBoxKmViagem.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 20);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(59, 13);
            this.label2.TabIndex = 0;
            this.label2.Text = "Km viagem";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(23, 272);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(48, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Km atual";
            // 
            // TextBoxKmAtual
            // 
            this.TextBoxKmAtual.Enabled = false;
            this.TextBoxKmAtual.Location = new System.Drawing.Point(186, 268);
            this.TextBoxKmAtual.Name = "TextBoxKmAtual";
            this.TextBoxKmAtual.Size = new System.Drawing.Size(100, 20);
            this.TextBoxKmAtual.TabIndex = 7;
            this.TextBoxKmAtual.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.ButtonAbastecer);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.TextBoxQttAbastecer);
            this.groupBox3.Controls.Add(this.LabelQttCombustivel);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.LabelReserva);
            this.groupBox3.Location = new System.Drawing.Point(314, 12);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(137, 173);
            this.groupBox3.TabIndex = 8;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Deposito";
            // 
            // ButtonAbastecer
            // 
            this.ButtonAbastecer.Enabled = false;
            this.ButtonAbastecer.Location = new System.Drawing.Point(50, 106);
            this.ButtonAbastecer.Name = "ButtonAbastecer";
            this.ButtonAbastecer.Size = new System.Drawing.Size(75, 23);
            this.ButtonAbastecer.TabIndex = 5;
            this.ButtonAbastecer.Text = "Abastecer";
            this.ButtonAbastecer.UseVisualStyleBackColor = true;
            this.ButtonAbastecer.Click += new System.EventHandler(this.ButtonAbastecer_Click);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(11, 72);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(63, 26);
            this.label6.TabIndex = 4;
            this.label6.Text = "Quantidade\r\na abastecer";
            // 
            // TextBoxQttAbastecer
            // 
            this.TextBoxQttAbastecer.Enabled = false;
            this.TextBoxQttAbastecer.Location = new System.Drawing.Point(81, 75);
            this.TextBoxQttAbastecer.Name = "TextBoxQttAbastecer";
            this.TextBoxQttAbastecer.Size = new System.Drawing.Size(45, 20);
            this.TextBoxQttAbastecer.TabIndex = 3;
            // 
            // LabelQttCombustivel
            // 
            this.LabelQttCombustivel.AutoSize = true;
            this.LabelQttCombustivel.Location = new System.Drawing.Point(89, 47);
            this.LabelQttCombustivel.Name = "LabelQttCombustivel";
            this.LabelQttCombustivel.Size = new System.Drawing.Size(30, 13);
            this.LabelQttCombustivel.TabIndex = 2;
            this.LabelQttCombustivel.Text = "0/00";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 47);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 13);
            this.label5.TabIndex = 1;
            this.label5.Text = "Combustivel";
            // 
            // LabelReserva
            // 
            this.LabelReserva.AutoSize = true;
            this.LabelReserva.BackColor = System.Drawing.SystemColors.Control;
            this.LabelReserva.Location = new System.Drawing.Point(27, 19);
            this.LabelReserva.Name = "LabelReserva";
            this.LabelReserva.Size = new System.Drawing.Size(80, 13);
            this.LabelReserva.TabIndex = 0;
            this.LabelReserva.Text = "Status Reserva";
            // 
            // LabelKmPossiveis
            // 
            this.LabelKmPossiveis.AutoSize = true;
            this.LabelKmPossiveis.Location = new System.Drawing.Point(33, 309);
            this.LabelKmPossiveis.Name = "LabelKmPossiveis";
            this.LabelKmPossiveis.Size = new System.Drawing.Size(99, 13);
            this.LabelKmPossiveis.TabIndex = 9;
            this.LabelKmPossiveis.Text = "Satus Km Possiveis";
            // 
            // LabelCustoViagem
            // 
            this.LabelCustoViagem.AutoSize = true;
            this.LabelCustoViagem.Location = new System.Drawing.Point(33, 328);
            this.LabelCustoViagem.Name = "LabelCustoViagem";
            this.LabelCustoViagem.Size = new System.Drawing.Size(106, 13);
            this.LabelCustoViagem.TabIndex = 10;
            this.LabelCustoViagem.Text = "Status Preço Viagem";
            // 
            // LabelCustoMedioKm
            // 
            this.LabelCustoMedioKm.AutoSize = true;
            this.LabelCustoMedioKm.Location = new System.Drawing.Point(33, 345);
            this.LabelCustoMedioKm.Name = "LabelCustoMedioKm";
            this.LabelCustoMedioKm.Size = new System.Drawing.Size(135, 13);
            this.LabelCustoMedioKm.TabIndex = 11;
            this.LabelCustoMedioKm.Text = "Status Custo Médio por Km";
            // 
            // LabelViagem
            // 
            this.LabelViagem.AutoSize = true;
            this.LabelViagem.Location = new System.Drawing.Point(33, 293);
            this.LabelViagem.Name = "LabelViagem";
            this.LabelViagem.Size = new System.Drawing.Size(75, 13);
            this.LabelViagem.TabIndex = 12;
            this.LabelViagem.Text = "Status Viagem";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(481, 372);
            this.Controls.Add(this.LabelViagem);
            this.Controls.Add(this.LabelCustoMedioKm);
            this.Controls.Add(this.LabelCustoViagem);
            this.Controls.Add(this.LabelKmPossiveis);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.TextBoxKmAtual);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox TextBoxMatricula;
        private System.Windows.Forms.Label LabelMatricula;
        private System.Windows.Forms.Label LabelCapacidadeDeposito;
        private System.Windows.Forms.TextBox TextBoxCapacidade;
        private System.Windows.Forms.Button ButtonRegistarCarro;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button ButtonRegistarKmViagem;
        private System.Windows.Forms.TextBox TextBoxKmViagem;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox TextBoxKmAtual;
        private System.Windows.Forms.TextBox TextBoxCustoMedioLitro;
        private System.Windows.Forms.TextBox TextBoxConsumoMedio;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label LabelConsumoMedio100Km;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TextBox TextBoxKmInicial;
        private System.Windows.Forms.Label LabelKmInicial;
        private System.Windows.Forms.Label LabelReserva;
        private System.Windows.Forms.Label LabelQttCombustivel;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button ButtonAbastecer;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox TextBoxQttAbastecer;
        private System.Windows.Forms.Label LabelKmPossiveis;
        private System.Windows.Forms.Label LabelCustoViagem;
        private System.Windows.Forms.Label LabelCustoMedioKm;
        private System.Windows.Forms.Label LabelViagem;
    }
}

